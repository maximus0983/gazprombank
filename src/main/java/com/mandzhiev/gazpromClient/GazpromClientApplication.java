package com.mandzhiev.gazpromClient;

import com.mandzhiev.gazpromClient.common.Pay;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class GazpromClientApplication implements ApplicationRunner {
    private static final double RANGE_MIN_RUB = 10_000.12;
    private static final double RANGE_MAX_RUB = 100_000.5;

    public static void main(String[] args) {
        SpringApplication.run(GazpromClientApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        System.out.println(args.getNonOptionArgs());
        String url = null;
        String offices = null;
        String file = null;
        int paysQuantity = 0;
        if (args.getNonOptionArgs().size() == 4) {
            offices = args.getNonOptionArgs().get(0);
            paysQuantity = Integer.parseInt(args.getNonOptionArgs().get(1));
            url = args.getNonOptionArgs().get(2);
            file = args.getNonOptionArgs().get(3);
        }
        if (url != null) {
            List<Pay> pays = generatePays(paysQuantity, readFile(offices));
            for (Pay pay1 : pays) {
                ResponseEntity<Pay> responseEntity = restTemplate.postForEntity(url + "/pay", pay1, Pay.class);
                Pay pay = responseEntity.getBody();
                final Path path = Paths.get(file);
                Files.write(path, Arrays.asList(pay.toString()), StandardCharsets.UTF_8,
                        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
            }
        }
    }

    private List<String> readFile(String file) throws IOException {
        return Files.readAllLines(Paths.get(file));
    }

    private List<Pay> generatePays(int paysQuantity, List<String> offices) {
        Random rnd = new Random();
        int index;
        long minDate;
        long maxDate;
        List<Pay> pays = new ArrayList<>();
        for (int i = 0; i < paysQuantity; i++) {
            index = rnd.nextInt(offices.size());
            String office = offices.get(index);
            double amount = ThreadLocalRandom.current().nextDouble(RANGE_MIN_RUB, RANGE_MAX_RUB);
            minDate = LocalDate.of(LocalDate.now().getYear() - 1, Month.JANUARY, 1).toEpochDay();
            maxDate = LocalDate.of(LocalDate.now().getYear(), Month.JANUARY, 1).toEpochDay();
            long randomDay = ThreadLocalRandom.current().nextLong(minDate, maxDate);
            LocalDate date = LocalDate.ofEpochDay(randomDay);
            long randomTime = ThreadLocalRandom.current().nextLong(LocalTime.of(23, 59, 59)
                    .toSecondOfDay());
            LocalTime time = LocalTime.ofSecondOfDay(randomTime);
            pays.add(new Pay(date, amount, office, time));
        }
        return pays;
    }
}