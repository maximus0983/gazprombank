package com.mandzhiev.gazpromClient.common;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class Pay implements Serializable {
    private long id;
    private LocalDate localDate;
    private double amount;
    private String pointOfSale;
    private double commission;
    private LocalTime time;

    public Pay(LocalDate localDate, double amount, String pointOfSale, LocalTime time) {
        this.localDate = localDate;
        this.time = time;
        this.amount = amount;
        this.pointOfSale = pointOfSale;
    }

    public Pay() {
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPointOfSale() {
        return pointOfSale;
    }

    public void setPointOfSale(String pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "Pay{" +
                "id=" + id +
                ", localDate=" + localDate +
                ", amount=" + amount +
                ", pointOfSale='" + pointOfSale + '\'' +
                ", commission=" + commission +
                ", time=" + time +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pay pay = (Pay) o;
        return id == pay.id &&
                Double.compare(pay.amount, amount) == 0 &&
                Double.compare(pay.commission, commission) == 0 &&
                Objects.equals(localDate, pay.localDate) &&
                Objects.equals(pointOfSale, pay.pointOfSale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, localDate, amount, pointOfSale, commission);
    }
}
